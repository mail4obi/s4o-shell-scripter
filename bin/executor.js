const { get, trim } = require('lodash');
const scp2 = require('scp2');

const ssh = require('../libs/ssh');

const defaultInputTimeout = 10;

/**
 * 
 * @param {String} data 
 * @param {{}} args 
 */
const parseVariables = (data, args) => {
  const varRegex = /\$[\w-\[\]]{1,}/ig;
  const match = data.match(varRegex);
  let text = data;
  if (!match) return text;
  match.forEach((variable) => {
    text = text.replace(variable, get(args, variable.substr(1), variable));
  });
  return text;
};

class Executor {
  static commandRegex = /(\w{1,}@((?:\*|\S){1,}))>\s{0,}(.*)/i;
  static inputRegex = /(\w{1,}@((?:\*|\S){1,}))>\s{1,}<\s{1,}(.*)/i;
  static sshRegex = /(\w{1,}@((?:\*|\S){1,}))>\s{1,}ssh\s{1,}(.*)/i;
  static scpRegex = /@@>\s{1,}scp\s{1,}(.*)/i;
  static variableRegex = /<-\s{0,}(\w{1,})\[(.*)\]\s{0,}=\s{0,}(.*)/i;
  static conditionRegex = /<%\s{0,}(.*)/i;

  constructor(env) {
    this.env = env;
  }

  sshConnections = {};
  collectOutputs = false;
  currentOutputVariable = undefined;
  currentOutputRegex = undefined;
  currentOutputs = [];

  waitForPrompt(id, text, timeout = defaultInputTimeout) {
    let count = 0;
    return new Promise((resolve, reject) => {
      const task = () => {
        // console.log('waiting for =>', id, text);
        if (String(this.sshConnections[id].lastData).indexOf(text) >= 0) {
          if (this.collectOutputs) {
            const regex = new RegExp(this.currentOutputRegex);
            const match = this.currentOutputs.join('').match(regex);
            this.env[this.currentOutputVariable] = match;
          }
          this.sshConnections[id].lastData = '';
          this.currentOutputs = [];
          this.collectOutputs = false;
          return resolve();
        }
        if (timeout > 0 && count > timeout) {
          reject('input await timeout');
        }
        count++;
        setTimeout(task, 1000);
      }
      task();
    });
  }

  async executeSSH(data) {
    const match = Executor.sshRegex.exec(data);
    const id = match[1];
    const hostName = match[2];
    const [userAndPassword, hostAndIp] = match[3].split('@');
    const [username, password] = userAndPassword.split(':');
    const [host, port] = hostAndIp.split(' ');
    const connectionOpts = {
      host: parseVariables(host, this.env),
      port: parseVariables(port, this.env),
      username: parseVariables(username, this.env),
      password: parseVariables(password, this.env),
    };
    const con = await ssh.getStream(connectionOpts);
    this.sshConnections[id] = con;
    con.stream.on('data', (data) => {
      this.sshConnections[id].lastData = data;
      if (this.collectOutputs) this.currentOutputs.push(data);
    });
    return { id, hostName };
  }

  executeSCP(data) {
    const match = Executor.scpRegex.exec(data);
    const commands = match[1].split(/\s{1,}/);
    const file1 = parseVariables(commands[0], this.env);
    const file2 = parseVariables(commands[1], this.env);
    console.log("files => ", [file1, file2]);
    return new Promise((resolve, reject) => {
      scp2.scp(file1, file2, (err) => {
        if (err) reject(err);
        else resolve();
      });
    });
  }

  processVariables(command) {
    if (Executor.variableRegex.test(command)) {
      const match = String(command).match(Executor.variableRegex);
      this.collectOutputs = true;
      this.currentOutputVariable = match[1];
      this.currentOutputRegex = match[2];
      return match[3];
    }

    return command;
  }

  processConditions(command) {
    if (Executor.conditionRegex.test(command)) {
      const match = String(command).match(Executor.conditionRegex);
      const sections = parseVariables(trim(match[1]), this.env);
      const $vars = this.env;
      const $set = (key, value) => {
        this.env[key] = value;
      };
      return eval(sections);
    }
  }

  async executeCommand(data) {
    const match = Executor.commandRegex.exec(data);
    const id = match[1];
    const hostName = match[2];
    const con = this.sshConnections[id];
    let input = match[3];
    input = this.processVariables(input);
    const sections = this.processConditions(input);
    if (sections) {
      return { id, hostName, sections };
    } else {
      const parsedInput = parseVariables(trim(input), this.env);
      con.stream.write(`${parsedInput}\n`);
      return { id, hostName };
    }
  }

  async executeInput(id, input) {
    const con = this.sshConnections[id];
    const parsedInput = parseVariables(trim(input), this.env);
    con.stream.write(`${parsedInput}\n`);
    return { id, input };
  }

  getInput(data) {
    const match = Executor.inputRegex.exec(data);
    const id = match[1];
    const hostName = match[2];
    let [matcher, ...input] = match[3].split(' ');
    input = input.join(' ');
    let timeout = defaultInputTimeout;
    const timeRegex = /.*--t\s{1,}\d{1,}/;
    if (timeRegex.test(input)) {
      const regex = /(.*)\s{1,}--t\s{1,}(\d{1,})/;
      const match = input.match(regex);
      input = match[1];
      timeout = Number(match[2]);
    }
    const result = { id, hostName, matcher, input, timeout };
    // console.log(result);
    return result;
  }
}

module.exports = Executor;
