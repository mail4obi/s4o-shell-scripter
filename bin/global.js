#!/usr/bin/env node

const fs = require('fs');
const { flattenDeep, pick, values } = require('lodash');

const Executor = require('./executor');

const args = process.argv;
const inputOpt = args.indexOf('--input');
const sectionOpt = args.indexOf('--sections');
const envOpt = args.indexOf('--env');

if (inputOpt < 0) {
  console.log('input option not found');
  process.exit(-1);
}
if (envOpt < 0) {
  console.log('env option not found');
  process.exit(-1);
}
if (!fs.existsSync(args[inputOpt + 1])) {
  console.log('input file not found');
  process.exit(-1);
}
if (!fs.existsSync(args[envOpt + 1])) {
  console.log('env json file not found');
  process.exit(-1);
}

const inputData = fs.readFileSync(args[inputOpt + 1], { encoding: 'utf-8' });
const evnData = fs.readFileSync(args[envOpt + 1], { encoding: 'utf-8' });
const sectionsData = sectionOpt ? String(args[sectionOpt + 1]).split(',').map((s) => s.trim()) : '*';

let lines = inputData.split('\n');

const sections = {
  _default0: [],
};

let currentSection = '_default0';
let defaultCount = 0;
const sectionRegex = /#@:\s{0,}([\w-]{1,})/i;
lines.forEach((line, i) => {
  if (sectionRegex.test(line)) {
    const match = line.match(sectionRegex);
    const section = match[1];
    if (section === '_') {
      defaultCount++;
      currentSection = `_default${defaultCount}`;
    }
    else currentSection = section;
  } else {
    if (!sections[currentSection]) sections[currentSection] = [];
    sections[currentSection].push(line);
  }
});

console.log(sections);

lines = [];
const sectionKeys = Object.keys(sections);
sectionKeys.forEach((key) => {
  let push = sectionsData.indexOf('*') >= 0 || sectionsData.indexOf(key) >= 0 || key.match(/_default\d{1,}/);
  if (sectionsData.indexOf(`!${key}`) >= 0) push = false;
  if (push) lines.push(...sections[key]);
});

console.log(lines);

const sshRegex = Executor.sshRegex;
const scpRegex = Executor.scpRegex;
const commandRegex = Executor.commandRegex;
const inputRegex = Executor.inputRegex;

const inputLines = [];

lines.forEach((line, i) => {
  if (commandRegex.test(line)) inputLines.push(i);
});

const executor = new Executor(JSON.parse(evnData));

/**
 * 
 * @param {Array<String>} lines
 */
const task = async (lines) => {
  for (let i = 0; i < lines.length; i++) {
    try {
      const line = lines[i];
      if (line === 'exit') {
        process.exit(0);
      }
      if (line.length < 1) {
        continue;
      }
      if (line.startsWith('#')) {
        console.log('\n', line);
        continue;
      }
      if (sshRegex.test(line)) {
        const { id, hostName } = await executor.executeSSH(line);
        await executor.waitForPrompt(id, `@${hostName}`);
      }
      else if (scpRegex.test(line)) {
        await executor.executeSCP(line);
        console.log(line);
      }
      else if (inputRegex.test(line)) {
        const { id, input, hostName } = executor.getInput(line);
        await executor.executeInput(id, input);
        const nextLine = lines[i + 1];
        if (!inputRegex.test(nextLine)) {
          await executor.waitForPrompt(id, `@${hostName}`, 60 * 60);
        } else {
          const { matcher, timeout } = executor.getInput(nextLine);
          await executor.waitForPrompt(id, matcher, timeout);
        }
      }
      else if (commandRegex.test(line)) {
        const {
          id,
          hostName,
          sections: conditionSections,
        } = await executor.executeCommand(line);
        if (conditionSections) {
          const newLines = flattenDeep(values(pick(sections, conditionSections)));
          await task(newLines);
        } else {
          const nextLine = lines[i + 1];
          if (inputRegex.test(nextLine)) {
            const { matcher, timeout } = executor.getInput(nextLine);
            await executor.waitForPrompt(id, matcher, timeout);
          } else {
            await executor.waitForPrompt(id, `@${hostName}`, 60 * 60);
          }
        }
      }
    } catch (error) {
      console.error(error);
      process.exit(-1);
    }
  }
};

task(lines);
