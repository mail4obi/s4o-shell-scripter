const Client = require('ssh2').Client;

/**
 * 
 * @param {{
 * host: String,
 * port: Number,
 * username: String,
 * password: String
 * }} connectOpts 
 * 
 * @returns {Promise<{
 *  stream: ReadableStream,
 *  connection: Client
 * }>}
 */
const getStream = (connectOpts) => new Promise((resolve, reject) => {
  const conn = new Client();
  conn.on('ready', function () {
    console.log(`${connectOpts.username}@${connectOpts.host}:${connectOpts.port} connected`);
    conn.shell(function (err, stream) {
      if (err) throw err;
      stream.on('close', function () {
        console.log('Stream :: close');
        conn.end();
      }).on('data', function (data) {
        process.stdout.write(data);
      });
      resolve({
        stream,
        connection: conn,
      });
    });
  }).connect(connectOpts);
});

module.exports = {
  getStream,
};
