### installation
```
npm i -g git+https://gitlab.com/mail4obi/s4o-shell-scripter.git
```

### usage
```shell
s4o-shell-scripter --input commands.sh --env env.json [--sections [comma seperated section names]]
```

### example env file
{
  "password": "...",
  "server_ip": "..."
}

### reserved words
- #@:<section_name> - a section of command
- #@:_ - default section (this and sectionless parts will execute if no sections is placed in the command)
- < - this is for notifying that input will be requested next
- <- - this is for assigning the output filtered by a regex to a variable
- <% - this is for creating a condition that will be evaluated and whose output is an array of sections to execute
- $vars - this is used to access both stored output, and environment variables
- $set(key,value) - this is used to set a variable that can be reused

### example ssh command
```shell
1@bettapoint-v3> ssh dev:$password@$server_ip 22
1@bettapoint-v3> sudo docker exec -it bettapoint-app bash 
1@bettapoint-v3> < password $password
1@bettapoint-v3> pm2 status

#@:section1
1@bettapoint-v3> <- docker_up[active\s\(running\)] = service docker status | cat
1@bettapoint-v3> <% if ($vars.docker_up[0]) $set('match', $vars.docker_up[0]); ['section2', $vars.match];

#@:section2
1@bettapoint-v3> sudo docker exec -it bettapoint-app bash 
1@bettapoint-v3> pm2 status
1@bettapoint-v3> exit

#@:section3
1@bettapoint-v3> sudo docker exec -it bettapoint-app bash 
1@bettapoint-v3> pm2 status
1@bettapoint-v3> exit

#@:_
exit
```

### disclaimer
this tool is still under development, user's descretion is adviced
